<?php
  define('_ERRORS', array(
    'n1' => "Host not working. Maybe a missing data directory at the root of the web server?",
    'n2' => "No valid ip address found in network. Looks like arp-scan is not working. Check the connection, the interface name in config.php and the sudo rights for web user on arp-scan.",
    'n3' => "Unable to get directory listing",
    'n4' => "Unable to load text file",
    'n5' => "Unable to create directory on local host",
    'n6' => "Unable to download file"

  ));
?>
