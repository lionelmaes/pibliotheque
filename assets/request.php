<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
/*
* request.php
* handle all requests to:
* * get connected hosts on local network
* * get documents list from a specific host
* * parse meta data for a specific document
* * copy a specific document from distant host to localhost
*/
require('../config.php');
require('errors.php');
require('piblio.php');

$piblio = new Piblio();

$json = file_get_contents('php://input');
if($json != ''){
  $data = json_decode($json);
  try{
    switch($data->request){
      case 'getpeers':
        echo json_encode(array('result'=>0, 'output'=>$piblio->getPeers(true),
        'message' => 'Network scanning done'));

        return;
      case 'getlisting':
        $listing = array();
        foreach($data->host as $hostname => $ip){
          $listing = array_merge($listing, $piblio->getListing($ip));

        }
        echo json_encode(array('result'=>0, 'output'=>$listing,
        'message' => 'Listing done'));

        return;
      case 'getfiles':
        $files = $piblio->getFiles($data->url);
        echo json_encode(array('result'=>0, 'output' => $files, 'message' => 'Files list done'));
        return;
      case 'getmetas':
        $metas = $piblio->getMetas($data->url);
        echo json_encode(array('result'=>0, 'output' => $metas, 'message' => 'Metadatas loaded'));
        return;
      case 'download':
        $localUrl = $piblio->download($data->url);
        echo json_encode(array('result'=>0, 'output' => $localUrl, 'message' => 'Document downloaded on local host'));
        return;

    }
  }catch(Exception $e){
    echo json_encode(array('result'=>1, 'message'=>$e->getMessage()));
  }
}

?>
