function Messenger(logger){
  this.logger = logger;

  this.getPeers = async function(){
    let data = await this.getMessage({'request':'getpeers'});
    return {'peers':data.output, 'message':data.message};
  }
  this.getListing = async function(host){
    let data = await this.getMessage({'request':'getlisting', 'host':host});
    return {'listing':data.output, 'message':data.message};
  }
  this.getFiles = async function(url){
    let data = await this.getMessage({'request':'getfiles', 'url':url});
    return {'files':data.output, 'message':data.message};
  }
  this.getMetas = async function(url){
    let data = await this.getMessage({'request':'getmetas', 'url':url});
    return {'metas':data.output, 'message':data.message};
  }
  this.download = async function(url){
    let data = await this.getMessage({'request':'download', 'url':url});
    return {'url':data.output, 'message':data.message};
  }
  this.getMessage = async function(message){
    let response = await fetch('assets/request.php', {
      method: 'post',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(message)
      });

      let res = await response.json();

      if(res.result == 0)
        return res;

      else{
        this.logger.error(res.message);
        throw new Error(res.message);
      }

  }
}

function Logger(){
  const domElem = document.getElementById('logger');

  this.log = function(message, level, object = null){
    if(object != null)
      message += ' :: '+JSON.stringify(object);
    let logLine = '<p class="logline '+level+'">'+message+'</p>';
    domElem.innerHTML += logLine;
    domElem.scrollTop = domElem.scrollHeight;
  }
  this.error = function(message, object = null){
    this.log(message, 'error', object);

  }
  this.info = function(message, object = null){
    this.log(message, 'info', object);
  }
}

function Piblio(){
  const logger = new Logger();
  const messenger = new Messenger(logger);
  const domElem = document.getElementById('documents');
  let peers = [];


  this.updatePeers = async function(){
    logger.info('Looking for peers');
    let data = await messenger.getPeers();

    peers = data.peers;
    logger.info(data.message, data.peers);


  }

  this.updateAllListings = async function(){
    await this.updateListing(peers.local, true);
    await Promise.all(peers.distant.map(async (peer) => {
      await this.updateListing(peer, false);
    }));
  }

  this.updateListing = async function(peer, local){
    logger.info('Updating listing', peer);
    const data = await this.getListing(peer);
    this.updateDOMListing(peer, data.listing, local);
    this.updateDownloadLinks();
  }
  this.getListing = async function(peer){
    logger.info('Looking for listing', peer);
    const data = await messenger.getListing(peer);
    logger.info(data.message, peer);
    return data;
  }

  this.updateDOMListing = function(peer, listing, local = false){

    const peerClass = (local)?'peer-local':'peer-'+this.strToClassName(Object.keys(peer)[0]);

    this.removeListing(peerClass);
    this.addListing(Object.keys(peer)[0], peerClass, listing);

  }
  this.removeListing = function(peerClass){

    const items = document.querySelectorAll('.'+peerClass);
    items.forEach(item => {
      //here we should remove the item and check if its parent ul is empty.
      //if it is the case, we should also remove the parent work (parent li of parent ul)
      let list = item.parentNode;
      list.removeChild(item);
      if(list.children.length == 0){
        let doc = list.parentNode;
        doc.parentNode.removeChild(doc);
      }

    });

  }
  this.addListing = function(peerName, peerClass, listing){

    //each work is a ul with each peer as li
    listing.forEach(item => {
      let parentDocClasses = ['title-'+this.strToClassName(item.infos.title)];
      if(item.infos.authors != undefined){
        item.infos.authors.forEach(author => {
          parentDocClasses.push('author-'+this.strToClassName(author));
        });
      }
      let parentDoc, parentList;

      //check if the document is already listed
      let existingDocument = domElem.querySelectorAll('.document.'+parentDocClasses.join('.'));
      if(existingDocument.length == 0){
        parentDoc = document.createElement('li');
        parentDoc.className = parentDocClasses.join(' ')+' document';
        parentDoc.innerHTML = item.infos.title;
        if(item.infos.authors != undefined && item.infos.authors.length > 0){
          let authorsP = document.createElement('p');
          authorsP.className = 'authors';
          item.infos.authors.forEach(author =>{
            authorsP.innerHTML += '<span class="author">'+author+'</span>';
          });
          parentDoc.appendChild(authorsP);
        }
        parentList = document.createElement('ul');
        parentDoc.appendChild(parentList);
        domElem.appendChild(parentDoc);
      }else {
        parentDoc = existingDocument[0];
        parentList = parentDoc.querySelector('ul');
      }


      let elem = document.createElement('li');
      elem.className = peerClass+' instance';
      elem.setAttribute('data-url', item.dirpath);
      elem.innerHTML = peerName;
      parentList.appendChild(elem);
    });
  }
  this.updateDownloadLinks = function(){
    let docs = domElem.querySelectorAll('.document');
    docs.forEach(doc => {

      //if there is no local copy of this document, insert download link for each peer
      if(doc.querySelector('.instance.peer-local') == null){
        let instances = doc.querySelectorAll('.instance');
        if(instances !== null){
          instances.forEach(instance => {
            if(instance.querySelector('.download-link') == null){
              let dlLink = document.createElement('a');

              dlLink.className = 'download-link';
              dlLink.setAttribute('href', instance.getAttribute('data-url'));
              dlLink.addEventListener('click', this.download.bind(this));
              instance.insertBefore(dlLink, instance.children[1]);
            }
          });
        }
      }else{
        let dlLink = doc.querySelector('.download-link');
        if(dlLink != null){
          dlLink.parentNode.removeChild(dlLink);
        }
      }
    });
  }
  this.updateAllFiles = async function(peersToUpdate = null){
    let selector = '.instance';
    if(peersToUpdate != null){
      peersToUpdate.forEach(peer => {

        if(Object.keys(peer)[0] == Object.keys(peers.local)[0])
          selector += '.peer-local';
        else
          selector+='.peer-'+this.strToClassName(Object.keys(peer)[0]);
      });
    }

    let instances = domElem.querySelectorAll(selector);

    await Promise.all(Array.from(instances).map(async(instance) => {
      await this.updateFiles(instance);
    }));
  }

  this.updateFiles = async function(instance){

    try{
      const data = await this.getFiles(instance);
      this.updateDOMFiles(instance, data.files);
    }catch(e){
      return;
    }


  }
  this.getFiles = async function(instance){
    logger.info('Looking for files @ '+instance.innerText, instance.getAttribute('data-url'));
    const data = await messenger.getFiles(instance.getAttribute('data-url'));
    logger.info(data.message, instance.getAttribute('data-url'));
    return data;
  }
  this.updateDOMFiles = function(instance, files){
    var list = document.createElement('ul');
    instance.appendChild(list);
    files.forEach(file => {
      let elem = document.createElement('li');

      elem.innerHTML = '<a href="'+file.path+'" class="file'+((file.name == 'meta.txt')?' meta':'')+'">'+file.name+'</a>';
      list.appendChild(elem);
    });
  }

  this.updateAllMetas = async function (peersToUpdate = null){
    let selector = '.instance';
    if(peersToUpdate != null){
      peersToUpdate.forEach(peer => {

        if(Object.keys(peer)[0] == Object.keys(peers.local)[0])
          selector += '.peer-local';
        else
          selector+='.peer-'+this.strToClassName(Object.keys(peer)[0]);
      });
    }

    let instances = domElem.querySelectorAll(selector);
    await Promise.all(Array.from(instances).map(async(instance) => {
      var metaLink = instance.querySelector('.meta');
      if(metaLink != null){
        await this.updateMetas(instance, metaLink.getAttribute('href'));
      }

    }));
  }
  this.updateMetas = async function(instance, metaURL){
    const data = await this.getMetas(metaURL);
    this.updateDOMMetas(instance, data.metas);
  }

  this.getMetas = async function(url){
    logger.info('Loading metas', url);
    const data = await messenger.getMetas(url);
    logger.info(data.message, url);
    return data;
  }
  this.updateDOMMetas = function(instance, metas){
    let doc = instance.parentNode.parentNode;
    let instanceList = instance.parentNode;
    let tagsDom = doc.querySelector(':scope > p.tags');
    if(tagsDom == null){
      tagsDom = document.createElement('p');
      tagsDom.className = 'tags';
      doc.insertBefore(tagsDom, instanceList);
    }
    metas.tags.forEach((tag) => {
      let tagClass = 'tag-'+this.strToClassName(tag);
      if(tagsDom.querySelector('.'+tagClass) == null){
        tagsDom.innerHTML += '<span class="'+tagClass+'">#'+tag+'</span> ';
      }
    });
    let infosDom = instance.querySelector('.infos');
    if(infosDom != null)
      instance.removeChild(infosDom);
    infosDom = document.createElement('div');
    infosDom.className = 'infos';
    instance.appendChild(infosDom);
    Object.keys(metas.infos).forEach((key) => {
      let infoClass = this.strToClassName(key);
      infosDom.innerHTML += '<p class="'+infoClass+'"><label>'+key+'</label> '+metas.infos[key]+'</p>';
    });

  }
  this.strToClassName = function(str){
    return str.replace(/[^a-z0-9]/g, function(s) {
        var c = s.charCodeAt(0);
        if (c == 32) return '-';
        if (c >= 65 && c <= 90) return '_' + s.toLowerCase();
        return '__' + ('000' + c.toString(16)).slice(-4);
    });
  }

  this.download = async function(e){
    e.preventDefault();
    console.log(e);
    let files = e.target.parentNode.querySelectorAll('.file');
    if(files == null){
      logger.error('No files to download');
      return;
    }
    let url = e.target.getAttribute('href');
    logger.info('Downloading document', url);
    const data = await messenger.download(url);
    logger.info(data.message, data.url);
    //TODO: we should just make a new instance and not updating all the listing files and metas of localhost
    //
    await this.updateListing(peers.local, true);
    await this.updateAllFiles([peers.local]);
    await this.updateAllMetas([peers.local]);
  }

  this.init = async function(){
    await this.updatePeers();
    await this.updateAllListings();
    await this.updateAllFiles();
    await this.updateAllMetas();
  }

}



const piblio = new Piblio();
piblio.init();
