<?php
class Piblio{
  public $br;

  public function __construct(){
      $this->br = new NetworkBrowser();
  }
  public function getPeers($update = false){
    if($update)
      $this->br->updateHosts();
    return array('local'=>$this->br->local, 'distant'=>$this->br->hosts);

  }

  public function getFiles($url){
    $links = $this->br->getLinksFromDir($url);
    $links = preg_grep('#.+?\.[^/]+#',$links);

    $files = array();

    foreach($links as $link){
      $files[] = array(
        'name' => $link,
        'path' => $url.'/'.$link

      );
    }
    return $files;
  }
  public function download($url){
    $this->br->updateLocal();
    $url = urldecode($url);
    $distURLParts = parse_url($url);
    $distHost = $distURLParts['host'];
    $dirName = basename($distURLParts['path']);

    if(!@mkdir('../data/'.$dirName, 0775))
      throw new Exception(_ERRORS['n5']);

    chmod('../data/'.$dirName, 0775);

    $files = $this->getFiles($url);

    foreach($files as $file){
      if(file_put_contents('../data/'.$dirName.'/'.$file['name'], fopen($file['path'], 'rb')) === FALSE){
        throw new Exception(_ERRORS['n6'].' '.$file['path']);
      }
      chmod('../data/'.$dirName.'/'.$file['name'], 0775);
    }

    return urlencode('http://'.reset($this->br->local).'/data/'.$dirName);

  }
  public function getMetas($url){
    $data = $this->br->getTextFile($url);
    $tags = array();
    $more = array();
    preg_match_all('/^#.*$/mi', $data, $tagsLines);
    foreach($tagsLines as $tagLine){
      $tagsA = explode('#', $tagLine[0]);
      foreach($tagsA as $tag){
        if(preg_match('/\p{L}/', $tag))
          $tags[] = rtrim($tag, ' ,');

      }
    }
    preg_match_all('/^\[\[(\p{L}*)(.*)\]\]/mis', $data, $infos);

    if(count($infos)>1){
      for($i = 0; $i < count($infos[1]); $i++){
        $more[$infos[1][$i]] = trim($infos[2][$i]);
      }
    }
    return array('tags' => $tags, 'infos' => $more);

  }
  public function getListing($ip){

    if(!$this->br->checkHost($ip))
      throw new Exception(_ERRORS['n1']);

    $listing = array();
    $links = $this->br->getLinksFromDir('http://'.$ip.'/data');
    $worksDir = preg_grep('#^.*?\p{L}+?.*?/$#',$links);

    foreach($worksDir as $workDir){
      $workDir = str_replace(array('./', '/'), '', $workDir);
      $dirName = rawurldecode($workDir);
      $listing[] = array(
        'dirname' => $dirName,
        'dirpath' => 'http://'.$ip.'/data/'.$workDir,
        'infos' => $this->getWorkInfo($dirName)
      );
    }

    return $listing;
  }
  public function getWorkInfo($dirName){
    $infos = array();
    $parts = explode('++', $dirName);
    //print_r($parts);
    if(count($parts) == 1){
      $infos['title'] = $parts[0];
    }else{
      $infos['title'] = $parts[1];
      $infos['authors'] = array();
      $authorsparts = explode('+', $parts[0]);
      foreach($authorsparts as $author){
        $infos['authors'][] = $author;
      }
    }
    return $infos;
  }



}

class NetworkBrowser{
  public $hosts, $local;

  public function __construct(){
      $this->hosts = array();

  }
  public function getLinksFromDir($url){

    $contents = @file_get_contents($url);
    if($contents === FALSE){
      throw new Exception(_ERRORS['n3']);
    }
    preg_match_All("|href=[\"'](.*?)[\"']|", $contents, $hrefs);
    return $hrefs[1];
  }
  public function getTextFile($url){
    $contents = @file_get_contents($url);
    if($contents === FALSE){
      throw new Exception(_ERRORS['n4']);
    }
    return $contents;
  }
  public function checkHost($ip){
    return $this->urlExists('http://'.$ip.'/data');
  }
  public function updateLocal(){
    $hostName = getHostName();
    $ip = getHostByName($hostName);
    if(!$this->checkHost($ip)){
      throw new Exception(_ERRORS['n1']);
    }
    $this->local = array($hostName=>$ip);
  }
  public function updateHosts(){
    $this->updateLocal();
    $cmd = 'sudo -S /usr/sbin/arp-scan --quiet --interface='._INTERFACE.' --localnet 2>&1';

    exec($cmd, $output);
    $scanSuccess = false;

    foreach($output as $outputLine){
       preg_match_all('#\d{1,3}\.\d{1,3}\.\d{1,3}.\d{1,3}#', $outputLine, $matches);
       if(count($matches[0]) > 0){
         $scanSuccess = true;



         if($this->checkHost($matches[0][0])){
           $host = @file_get_contents('http://'.$matches[0][0].'/assets/hostname.php');
           if($host === FALSE){
             $host = str_replace('.', '', $matches[0][0]);
           }
            if(isset($this->local[$host]))
              continue;
            $this->hosts[] = array($host => $matches[0][0]);

          }
       }
    }
    if(!$scanSuccess){
      throw new Exception(_ERRORS['n2']);
    }

  }
  public function urlExists($url){
    // Store previous default context

    $prev = stream_context_get_options(stream_context_get_default());

    // Set a small timeout
    stream_context_set_default([
      'http' => [
        'timeout' => 3, // seconds
      ]
    ]);
    $headers=@get_headers($url, true);
    //print_r($headers);
    if (!empty($headers)) {
      //echo $headers[0];

      $result = (stripos($headers[0],"200 OK")||stripos($headers[0],"301"))?true:false;

    }
    else $result = false;

    // Restore previous default context
    stream_context_set_default($prev);
    return $result;
  }
}
?>
