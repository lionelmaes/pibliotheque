<?php
  include('turbine.php');
  print_r($files);
?>



<!doctype html>
<html>
<head>
  <meta charset="UTF-8">
</head>
<body>
<ul>
  <?php

  foreach($files as $host => $works){
    echo '<li><h2>'.$host.'</h2><ul>';
    foreach($works as $work){
      echo '<li>';
      echo '<h3>'.$work['infos']['title'].'</h3>';
      echo '<p class="authors">';
      foreach($work['infos']['authors'] as $author){
        echo '<span class="author">'.$author.'</span>';

      }
      echo '</p>';
      echo '<p class="meta">'.$work['meta'].'</p>';
      echo '<ul class="files">';
      foreach($work['files'] as $file){
        echo '<li><a href="'.$file.'">'.urldecode($file).'</a></li>';

      }
      echo '</ul>';
      echo '</li>';
    }
    echo '</ul></li>';
  }
  ?>
</ul>
</body>
</html>
