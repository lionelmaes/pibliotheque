<?php
//["thomas", "elie", "laura", "alexia", "chloe"]


function URL_exists($url){
   $headers=get_headers($url);
   return stripos($headers[0],"200 OK")?true:false;
}

function getLinksFromListing($url){

  if(!URL_exists($url)) return false;
  $contents = file_get_contents($url);
  preg_match_All("|href=[\"'](.*?)[\"']|", $contents, $hrefs);
  return $hrefs[1];
}
function getWorkInfos($folderName){
  $infos = array();
  $folderName = urldecode($folderName);
  $parts = explode('++', $folderName);
  //print_r($parts);
  if(count($parts) == 1){
    $infos['title'] = $parts[0];
  }else{
    $infos['title'] = $parts[1];
    $infos['authors'] = array();
    $authorsparts = explode('+', $parts[0]);
    foreach($authorsparts as $author){
      $infos['authors'][] = $author;
    }
  }
  return $infos;
}

function getFilesList($hosts){
  $files = array();

  foreach($hosts as $host){
    $files[$host] = array();
    $baseURL = "http://$host.local/data/";

    $foldersLinks = getLinksFromListing($baseURL);
    if(!$foldersLinks)
      continue;

    foreach($foldersLinks as $folderLink){
      if(preg_match('#[^.]+?/#', $folderLink)){
        $folderName = str_replace('/', '', $folderLink);

        //print_r($workInfos);
        $work = array();
        $work['infos'] = getWorkInfos($folderName);


        $filesLinks = getLinksFromListing($baseURL.$folderLink);
        //echo $baseURL.$folderLink.'meta.txt';
        if(URL_exists($baseURL.$folderLink.'meta.txt')){
          //echo 'coucou';
          $work['meta'] = file_get_contents($baseURL.$folderLink.'meta.txt');
        }

        $work['files'] = array();
        foreach($filesLinks as $fileLink){
          if(preg_match('#.+?\.[^/]+#',$fileLink) && $fileLink != 'meta.txt'){
            $work['files'][] = $baseURL.$folderLink.$fileLink;
          }
        }

        $files[$host][] = $work;

      }
    }

  }
  return $files;
}

$hosts = json_decode(file_get_contents("hosts.json"), true);
$files = getFilesList($hosts);
?>
