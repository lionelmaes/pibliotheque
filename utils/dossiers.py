#!/usr/bin/env python
#coding: utf-8

##ATTENTION !##
##NE FONCTIONNE QUE S'IL Y A UN FICHIER PAR TEXTE (S'IL Y A UN EPUB ET UN PDF, LE SCRIPT FAIT UN DOSSIER POUR CHACU D'ENTRE EUX)##

import os, shutil

#remplacer 'path' par le chemin du dossier contenant les livres
path = 'path/'

#liste tous les fichiers dans ce dossier, crée un fichier nommé comme le fichier, et déplace le fichier en question dedans
dir = os.listdir(path)
for file in dir:
    os.mkdir(file)
    shutil.move(file, path+file+"/"+file)

#scanne chaque dossier, trouve le nom du fichier dedans, crée le fichier 'meta.txt' et ajoute la ligne avec le nom du fichier en question
dir = os.listdir(path)
for doss in dir:
    file = os.listdir(path+doss)[0]
    print(file)
    with open(path+doss+"/meta.txt", "w", encoding="utf-8") as f:
       f.write(file)
       f.close()
