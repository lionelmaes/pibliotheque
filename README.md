# pibliotheque

A local file sharing system between Raspberry Pi's. It is a project initiated in the context of the "digital practices" class at ERG, Brussels. It runs with JS and PHP on Raspberry PIS with NGINX installed. It continuously looks for other peers on the local network it's connected to and for each peer detected, it parses the directory `/data` at the root of the web server.
Every document in this directory must be in its own directory, with a name matching with the syntax: ̀`AUTHOR1 NAME+AUTHOR2 NAME++TITLE`.

This directory contains the file(s) of the document and a special file named `meta.txt` with the following syntax:

`#keyword1, #key word 2

[[comment
this is a comment about the document
]]`

This syntax is subjected to change to support new attributes and types of classification.

## Installation and configuration
* Pibliotheque need nginx and php to work.
* For peers discovery, you will have to install arp-scan on the raspberry
`sudo apt install arp-scan`
* Once arp-scan is installed, you will have to give sudo rights on it for the user www-data. Edit this file:
`sudo nano /etc/sudoers`
And add this line at the end of the file:
`www-data ALL =(root) NOPASSWD: /usr/sbin/arp-scan`
* For documents download, the `data` folder needs to be writable for the user group associated with the web server which is often `www-data`. First, change the owner of the data folder:
`sudo chown www-data:www-data data`
* Then, add the user pi to the www-data group so he will be able to modify the downloaded content:
`sudo useradd -g www-data pi`
* Finally, change the permissions on the data directory: `sudo chmod 775 data`
